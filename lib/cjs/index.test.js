"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = require("./index");
var MyCoolEnumError;
(function (MyCoolEnumError) {
    MyCoolEnumError["LetsGetThisBread"] = "lets get this bread!!";
    MyCoolEnumError["YetAnotherChickenDinner"] = "yet another chicken dinner";
})(MyCoolEnumError || (MyCoolEnumError = {}));
var getCoolEnumValueFromString = function (value) { return value; };
test("Expect Is Equal", function () {
    try {
        (function () { throw new Error(MyCoolEnumError.LetsGetThisBread); });
    }
    catch (e) {
        var lets_get_this_bread_error = new index_1.EnumError(e, getCoolEnumValueFromString);
        expect(lets_get_this_bread_error.type).toBe(MyCoolEnumError.LetsGetThisBread);
    }
});
