import { EnumError } from "./index";
var MyCoolEnumError;
(function (MyCoolEnumError) {
    MyCoolEnumError["LetsGetThisBread"] = "lets get this bread!!";
    MyCoolEnumError["YetAnotherChickenDinner"] = "yet another chicken dinner";
})(MyCoolEnumError || (MyCoolEnumError = {}));
var getCoolEnumValueFromString = function (value) { return value; };
test("Expect Is Equal", function () {
    try {
        (function () { throw new Error(MyCoolEnumError.LetsGetThisBread); });
    }
    catch (e) {
        var lets_get_this_bread_error = new EnumError(e, getCoolEnumValueFromString);
        expect(lets_get_this_bread_error.type).toBe(MyCoolEnumError.LetsGetThisBread);
    }
});
