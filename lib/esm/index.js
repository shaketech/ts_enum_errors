var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var EnumError = /** @class */ (function (_super) {
    __extends(EnumError, _super);
    function EnumError(base_error, error_from_string) {
        var _this = this;
        if (isError(base_error)) {
            var error_value = error_from_string(base_error.message);
            if (error_value) {
                _this = _super.call(this, error_value + " : " + base_error) || this;
                _this.type = error_value;
            }
        }
        else if (isString(base_error)) {
            var error_value = error_from_string(base_error);
            if (error_value) {
                _this = _super.call(this, error_value + " : " + base_error) || this;
                _this.type = error_value;
            }
        }
        //we don't know how to catch this error object so just throw it
        else
            throw base_error;
        return _this;
    }
    return EnumError;
}(Error));
export { EnumError };
//Pure Genius stolen From https://stackoverflow.com/questions/58278652/generic-enum-type-guard
//Creates a function that checks if a token is T[keyof T]
export var isSomeEnum = function (e) { return function (token) { return Object.values(e).includes(token); }; };
//Simple Error Typeguard
export var isError = function (toBeDetermined) { return toBeDetermined.message ? true : false; };
//Simple String Typeguard
function isString(value) {
    return typeof value === 'string' || value instanceof String;
}
