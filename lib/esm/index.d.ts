export declare class EnumError<V> extends Error {
    base_error: Error;
    type: V;
    constructor(base_error: any, error_from_string: (value: string) => V);
}
export declare const isSomeEnum: <T>(e: T) => (token: any) => token is T[keyof T];
export declare const isError: (toBeDetermined: any) => toBeDetermined is Error;
