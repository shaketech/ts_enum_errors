export class EnumError<V> extends Error
{
    public base_error : Error;
    public type : V;
    public constructor(base_error : any, error_from_string : (value: string) => V)
    {
        if(isError(base_error)){
            const error_value = error_from_string(base_error.message);
            if(error_value)
            {
                super(`${ error_value } : ${base_error}`);
                this.type = error_value;
            }
        }
        else if(isString(base_error))
        {
            const error_value = error_from_string(base_error);
            if(error_value)
            {
                super(`${ error_value } : ${base_error}`);
                this.type = error_value;
            }
        }
        //we don't know how to catch this error object so just throw it
        else throw base_error;

        
        
    }
}

//Pure Genius stolen From https://stackoverflow.com/questions/58278652/generic-enum-type-guard
//Creates a function that checks if a token is T[keyof T]
export const isSomeEnum = <T>(e: T) => (token: any): token is T[keyof T] => Object.values(e).includes(token as T[keyof T]);

//Simple Error Typeguard
export const isError = (toBeDetermined: any) : toBeDetermined is Error => (toBeDetermined as Error).message ? true : false;

//Simple String Typeguard
function isString(value : any) : value is string {
	return typeof value === 'string' || value instanceof String;
}