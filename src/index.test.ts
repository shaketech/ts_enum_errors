import { EnumError } from "./index";

enum MyCoolEnumError
{
    LetsGetThisBread = "lets get this bread!!",
    YetAnotherChickenDinner = "yet another chicken dinner"
}

const getCoolEnumValueFromString = (value : string ) => value as MyCoolEnumError;

test("Expect Is Equal", ()=>
{

    try{
        () => { throw new Error(MyCoolEnumError.LetsGetThisBread);  }
    }catch(e)
    {
        const lets_get_this_bread_error = new EnumError<MyCoolEnumError>(e, getCoolEnumValueFromString);
        expect(lets_get_this_bread_error.type).toBe(MyCoolEnumError.LetsGetThisBread);
    }
})